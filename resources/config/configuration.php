<?php

use Carbon\Carbon;
use Defr\ChartWidgetExtension\ChartWidgetFieldTypeGuesser;
use Defr\ChartWidgetExtension\ChartWidgetRegistry;

// 'start_date' => [
//     'type'   => 'anomaly.field_type.datetime',
//     'config' => [
//         'default_value' => Carbon::now()->subYear(),
//     ],
// ],
// 'end_date'   => [
//     'type'   => 'anomaly.field_type.datetime',
//     'config' => [
//         'default_value' => Carbon::now(),
//     ],
// ],
// 'method'     => [
//     'type'   => 'anomaly.field_type.select',
//     'config' => [
//         'default_value' => 'fetch_visitors_and_page_views',
//         'options'       => [
//             'fetch_visitors_and_page_views'       => 'Fetch Visitors And Page Views',
//             'fetch_total_visitors_and_page_views' => 'Fetch Total Visitors And Page Views',
//             'fetch_most_visited_pages'            => 'Fetch Most Visited Pages',
//             'fetch_top_referrers'                 => 'Fetch Top Referrers',
//             'fetch_top_browsers'                  => 'Fetch Top Browsers',
//             'perform_query'                       => 'Perform Query',
//         ],
//     ],
// ],

// $registry = app(ChartWidgetRegistry::class);

// foreach ($registry->getCharts() as $chart_slug => $chart_array)
// {
//     if (is_array($chart_array))
//     {
//         foreach ($chart_array as $field_slug => $field_value)
//         {
//             if (!is_array($field_value))
//             {
//                 // dd(app('events'));
//             }
//         }
//     }
// }

return [
    'labels'  => [
        'type'   => 'anomaly.field_type.slider',
        'config' => [
            'min'           => 2,
            'max'           => 32,
            'default_value' => 8,
        ],
    ],
    'chart'   => [
        'type'     => 'anomaly.field_type.select',
        'required' => true,
        'config'   => [
            'options' => function (ChartWidgetRegistry $charts)
            {
                return array_keys($charts->getCharts());
            },
        ],
    ],
    'options' => [],
];
