<?php namespace Defr\ChartWidgetExtension;

/**
 * Class ChartWidgetRegistry
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 *
 * @link   http://pyrocms.com/
 */
class ChartWidgetRegistry
{

    /**
     * Available chart.
     *
     * @var array
     */
    protected $charts = [
        'line' => [
            // Options for X-Axis
            'axisX'              => [
                // The offset of the labels to the chart area
                'offset'                => 30,
                // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                'position'              => 'end',
                // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                'labelOffset'           => [
                    'x' => 0,
                    'y' => 0,
                ],
                // If labels should be shown or not
                'showLabel'             => true,
                // If the axis grid should be drawn or not
                'showGrid'              => true,
                // Interpolation function that allows you to intercept the value from the axis label
                'labelInterpolationFnc' => 'Chartist.noop',
                // Set the axis type to be used to project values on this axis. If not defined, Chartist.StepAxis will be used for the X-Axis, where the ticks option will be set to the labels in the data and the stretch option will be set to the global fullWidth option. This type can be changed to any axis constructor available (e.g. Chartist.FixedScaleAxis), where all axis options should be present here.
                'type'                  => null,
            ],
            // Options for Y-Axis
            'axisY'              => [
                // The offset of the labels to the chart area
                'offset'                => 40,
                // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                'position'              => 'start',
                // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                'labelOffset'           => [
                    'x' => 0,
                    'y' => 0,
                ],
                // If labels should be shown or not
                'showLabel'             => true,
                // If the axis grid should be drawn or not
                'showGrid'              => true,
                // Interpolation function that allows you to intercept the value from the axis label
                'labelInterpolationFnc' => 'Chartist.noop',
                // Set the axis type to be used to project values on this axis. If not defined, Chartist.AutoScaleAxis will be used for the Y-Axis, where the high and low options will be set to the global high and low options. This type can be changed to any axis constructor available (e.g. Chartist.FixedScaleAxis), where all axis options should be present here.
                'type'                  => null,
                // This value specifies the minimum height in pixel of the scale steps
                'scaleMinSpace'         => 20,
                // Use only integer values (whole numbers) for the scale steps
                'onlyInteger'           => false,
            ],
            // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
            'width'              => null,
            // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
            'height'             => null,
            // If the line should be drawn or not
            'showLine'           => true,
            // If dots should be drawn or not
            'showPoint'          => true,
            // If the line chart should draw an area
            'showArea'           => false,
            // The base for the area chart that will be used to close the area shape (is normally 0)
            'areaBase'           => 0,
            // Specify if the lines should be smoothed. This value can be true or false where true will result in smoothing using the default smoothing interpolation function Chartist.Interpolation.cardinal and false results in Chartist.Interpolation.none. You can also choose other smoothing / interpolation functions available in the Chartist.Interpolation module, or write your own interpolation function. Check the examples for a brief description.
            'lineSmooth'         => true,
            // If the line chart should add a background fill to the .ct-grids group.
            'showGridBackground' => false,
            // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
            'low'                => null,
            // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
            'high'               => null,
            // Padding of the chart drawing area to the container element and labels as a number or 'padding' object {top => 5, right => 5, bottom => 5, left => 5]
            'chartPadding'       => [
                'top'    => 15,
                'right'  => 15,
                'bottom' => 5,
                'left'   => 10,
            ],
            // When set to true, the last grid line on the x-axis is not drawn and the chart elements will expand to the full available width of the chart. For the last label to be drawn correctly you might need to add chart padding or offset the last label with a draw event handler.
            'fullWidth'          => false,
            // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
            'reverseData'        => false,
            // Override the class names that get used to generate the SVG structure of the chart
            'classNames'         => [
                'chart'          => 'ct-chart-line',
                'label'          => 'ct-label',
                'labelGroup'     => 'ct-labels',
                'series'         => 'ct-series',
                'line'           => 'ct-line',
                'point'          => 'ct-point',
                'area'           => 'ct-area',
                'grid'           => 'ct-grid',
                'gridGroup'      => 'ct-grids',
                'gridBackground' => 'ct-grid-background',
                'vertical'       => 'ct-vertical',
                'horizontal'     => 'ct-horizontal',
                'start'          => 'ct-start',
                'end'            => 'ct-end',
            ],
        ],
        'bar'  => [
            // Options for X-Axis
            'axisX'              => [
                // The offset of the chart drawing area to the border of the container
                'offset'                => 30,
                // Position where labels are placed. Can be set
                // to `start` or `end` where `start` is equivalent
                // to left or top on vertical axis and `end` is equivalent
                // to right or bottom on horizontal axis.
                'position'              => 'end',
                // Allows you to correct label positioning on this axis
                // by positive or negative x and y offset.
                'labelOffset'           => [
                    'x' => 0,
                    'y' => 0,
                ],
                // If labels should be shown or not
                'showLabel'             => true,
                // If the axis grid should be drawn or not
                'showGrid'              => true,
                // Interpolation function that allows you to
                // intercept the value from the axis label
                'labelInterpolationFnc' => 'Chartist.noop',
                // This value specifies the minimum width in
                // pixel of the scale steps
                'scaleMinSpace'         => 30,
                // Use only integer values (whole numbers)
                // for the scale steps
                'onlyInteger'           => false,
            ],
            // Options for Y-Axis
            'axisY'              => [
                // The offset of the chart drawing area
                // to the border of the container
                'offset'                => 40,
                // Position where labels are placed. Can be set
                // to `start` or `end` where `start` is equivalent
                // to left or top on vertical axis and `end` is equivalent
                // to right or bottom on horizontal axis.
                'position'              => 'start',
                // Allows you to correct label positioning on this
                // axis by positive or negative x and y offset.
                'labelOffset'           => [
                    'x' => 0,
                    'y' => 0,
                ],
                // If labels should be shown or not
                'showLabel'             => true,
                // If the axis grid should be drawn or not
                'showGrid'              => true,
                // Interpolation function that allows you to intercept the value from the axis label
                'labelInterpolationFnc' => 'Chartist.noop',
                // This value specifies the minimum height in pixel of the scale steps
                'scaleMinSpace'         => 20,
                // Use only integer values (whole numbers) for the scale steps
                'onlyInteger'           => false,
            ],
            // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
            'width'              => null,
            // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
            'height'             => null,
            // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
            'high'               => null,
            // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
            'low'                => null,
            // Unless low/high are explicitly set, bar chart will be centered at zero by default. Set referenceValue to null to auto scale.
            'referenceValue'     => 0,
            // Padding of the chart drawing area to the container element and labels as a number or 'padding' object [top => 5, right => 5, bottom => 5, left => 5]
            'chartPadding'       => [
                'top'    => 15,
                'right'  => 15,
                'bottom' => 5,
                'left'   => 10,
            ],
            // Specify the distance in pixel of bars in a group
            'seriesBarDistance'  => 15,
            // If set to true this property will cause the series bars to be stacked. Check the `stackMode` option for further stacking options.
            'stackBars'          => false,
            // If set to 'overlap' this property will force the stacked bars to draw from the zero line.
            // If set to 'accumulate' this property will form a total for each series point. This will also influence the y-axis and the overall bounds of the chart. In stacked mode the seriesBarDistance property will have no effect.
            'stackMode'          => 'accumulate',
            // Inverts the axes of the bar chart in order to draw a horizontal bar chart. Be aware that you also need to invert your axis settings as the Y Axis will now display the labels and the X Axis the values.
            'horizontalBars'     => false,
            // If set to true then each bar will represent a series and the data array is expected to be a one dimensional array of data values rather than a series array of series. This is useful if the bar chart should represent a profile rather than some data over time.
            'distributeSeries'   => false,
            // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
            'reverseData'        => false,
            // If the bar chart should add a background fill to the .ct-grids group.
            'showGridBackground' => false,
            // Override the class names that get used to generate the SVG structure of the chart
            'classNames'         => [
                'chart'          => 'ct-chart-bar',
                'horizontalBars' => 'ct-horizontal-bars',
                'label'          => 'ct-label',
                'labelGroup'     => 'ct-labels',
                'series'         => 'ct-series',
                'bar'            => 'ct-bar',
                'grid'           => 'ct-grid',
                'gridGroup'      => 'ct-grids',
                'gridBackground' => 'ct-grid-background',
                'vertical'       => 'ct-vertical',
                'horizontal'     => 'ct-horizontal',
                'start'          => 'ct-start',
                'end'            => 'ct-end',
            ],
        ],
        'pie'  => [
            // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
            'width'                 => null,
            // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
            'height'                => null,
            // Padding of the chart drawing area to the container element and labels as a number or 'padding' object [top => 5, right => 5, bottom => 5, left => 5]
            'chartPadding'          => 5,
            // Override the class names that are used to generate the SVG structure of the chart
            'classNames'            => [
                'chartPie'   => 'ct-chart-pie',
                'chartDonut' => 'ct-chart-donut',
                'series'     => 'ct-series',
                'slicePie'   => 'ct-slice-pie',
                'sliceDonut' => 'ct-slice-donut',
                'label'      => 'ct-label',
            ],
            // The start angle of the pie chart in degrees where 0 points north. A higher value offsets the start angle clockwise.
            'startAngle'            => 0,
            // An optional total you can specify. By specifying a total value, the sum of the values in the series must be this total in order to draw a full pie. You can use this parameter to draw only parts of a pie or gauge charts.
            'total'                 => null,
            // If specified the donut CSS classes will be used and strokes will be drawn instead of pie slices.
            'donut'                 => false,
            // Specify the donut stroke width, currently done in javascript for convenience. May move to CSS styles in the future.
            // This option can be set as number or string to specify a relative width (i.e. 100 or '30%').
            'donutWidth'            => 60,
            // If a label should be shown or not
            'showLabel'             => true,
            // Label position offset from the standard position which is half distance of the radius. This value can be either positive or negative. Positive values will position the label away from the center.
            'labelOffset'           => 0,
            // This option can be set to 'inside', 'outside' or 'center'. Positioned with 'inside' the labels will be placed on half the distance of the radius to the border of the Pie by respecting the 'labelOffset'. The 'outside' option will place the labels at the border of the pie and 'center' will place the labels in the absolute center point of the chart. The 'center' option only makes sense in conjunction with the 'labelOffset' option.
            'labelPosition'         => 'inside',
            // An interpolation function for the label value
            'labelInterpolationFnc' => 'Chartist.noop',
            // Label direction can be 'neutral', 'explode' or 'implode'. The labels anchor will be positioned based on those settings as well as the fact if the labels are on the right or left side of the center of the chart. Usually explode is useful when labels are positioned far away from the center.
            'labelDirection'        => 'neutral',
            // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
            'reverseData'           => false,
            // If true empty values will be ignored to avoid drawing unncessary slices and labels
            'ignoreEmptyValues'     => false,
        ],
    ];

    /**
     * Get a chart.
     *
     * @param  $chart
     * @return string
     */
    public function get($chart)
    {
        return array_get($this->charts, $chart, $chart);
    }

    /**
     * Register a chart.
     *
     * @param  $chart
     * @param  array    $parameters
     * @return $this
     */
    public function register($chart, array $parameters)
    {
        array_set($this->charts, $chart, $parameters);

        return $this;
    }

    /**
     * Get the charts.
     *
     * @return array
     */
    public function getCharts()
    {
        return $this->charts;
    }

    /**
     * Set the charts.
     *
     * @param  array   $charts
     * @return $this
     */
    public function setCharts(array $charts)
    {
        $this->charts = $charts;

        return $this;
    }
}
