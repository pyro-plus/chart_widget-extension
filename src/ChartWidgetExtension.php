<?php namespace Defr\ChartWidgetExtension;

use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Anomaly\DashboardModule\Widget\Extension\WidgetExtension;
use Defr\ChartWidgetExtension\Command\LoadItems;

/**
 * Class WidgetExtension
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 *
 * @link          http://pyrocms.com/
 */
class ChartWidgetExtension extends WidgetExtension
{

    /**
     * This extension provides
     *
     * @var null|string
     */
    protected $provides = 'anomaly.module.dashboard::widget.chart';

    /**
     * Load the widget data.
     *
     * @param WidgetInterface $widget
     */
    protected function load(WidgetInterface $widget)
    {
        $this->dispatch(new LoadItems($widget));
    }
}
