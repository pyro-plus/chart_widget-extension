<?php namespace Defr\ChartWidgetExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use PragmaRX\Tracker\Vendor\Laravel\Facade;
use PragmaRX\Tracker\Vendor\Laravel\Middlewares\Tracker;
use PragmaRX\Tracker\Vendor\Laravel\ServiceProvider;

// use Spatie\Analytics\AnalyticsServiceProvider;

class ChartWidgetExtensionServiceProvider extends AddonServiceProvider
{

    // protected $providers = [
    //     ServiceProvider::class,
    // ];

    // protected $middleware = [
    //     Tracker::class,
    // ];

    // protected $plugins    = [];
    // protected $commands   = [];
    // protected $routes     = [];
    // protected $listeners  = [];
    // protected $bindings   = [
    //     'Tracker' => Facade::class,

    // ];
    // protected $singletons = [];
    protected $overrides  = [
        'module::admin/widgets/choose' => 'defr.extension.chart_widget::choose',
    ];
    // protected $mobile     = [];

    // public function register()
    // {

    //     }

    // public function map()
    // {
    //     }

    // /**
    //  * Boot the service provider.
    //  *
    //  * @param Repository $config
    //  */
    // public function boot(Repository $config, Application $app)
    // {
    //     $config->set('tracker', $config->get('defr.extension.chart_widget::tracker'));
    // }
}
