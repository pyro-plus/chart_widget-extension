<?php namespace Defr\ChartWidgetExtension\Command;

use Anomaly\ConfigurationModule\Configuration\Contract\ConfigurationRepositoryInterface;
use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Defr\ChartWidgetExtension\ChartWidgetRegistry;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class LoadItems
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 *
 * @link   http://pyrocms.com/
 */
class LoadItems
{

    use DispatchesJobs;

    /**
     * The widget instance.
     *
     * @var WidgetInterface
     */
    protected $widget;

    /**
     * Create a new LoadItems instance.
     *
     * @param WidgetInterface $widget
     */
    public function __construct(WidgetInterface $widget)
    {
        $this->widget = $widget;
    }

    /**
     * Handle the widget data.
     *
     * @param Repository                       $cache
     * @param ChartWidgetRegistry              $charts        The charts
     * @param ConfigurationRepositoryInterface $configuration The configuration
     */
    public function handle(
        Repository $cache,
        ChartWidgetRegistry $charts,
        ConfigurationRepositoryInterface $configuration
    )
    {
        $options = array_merge(
            $charts->get(
                $configuration->value(
                    'defr.extension.chart_widget::chart',
                    $this->widget->getId()
                )
            ),
            []
            // $configuration->value(
            //     'defr.extension.chart_widget::options',
            //     $this->widget->getId()
            // )
        );

        $this->configuration = $configuration;

        $items = $cache->remember(
            __METHOD__.'_'.$this->widget->getId(),
            30,
            function ()
            {
                return [
                    'labels'  => $this->configuration->value(
                        'defr.extension.chart_widget::labels',
                        $this->widget->getId()
                    ),
                    'series'  => $this->configuration->value(
                        'defr.extension.chart_widget::series',
                        $this->widget->getId()
                    ),
                ];
            }
        );

        // Load the items to the widget's view data.
        $this->widget->addData('items', $items);
    }
}
