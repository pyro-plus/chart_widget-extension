<?php namespace Defr\ChartWidgetExtension;

use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Anomaly\DashboardModule\Widget\Extension\WidgetExtension;
use Defr\ChartWidgetExtension\Command\LoadItems;

/**
 * Class ChartWidgetFieldTypeGuesser
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 *
 * @link          http://pyrocms.com/
 */
class ChartWidgetFieldTypeGuesser
{

    /**
     * This field slug
     *
     * @var null|string
     */
    protected $slug;

    /**
     * Value to guess
     *
     * @var mixed
     */
    protected $value;

    /**
     * Prefix
     *
     * @var string
     */
    protected $prefix;

    /**
     * Create an instance of guesser class
     *
     * @param string $prefix The prefix
     * @param string $slug   The slug
     * @param mixed  $value  The value
     */
    public function __construct($prefix = '', $slug, $value)
    {
        $this->slug   = $slug;
        $this->value  = $value;
        $this->prefix = $prefix;
    }

    /**
     * Handle guess
     *
     * @return array
     */
    public function handle()
    {
        if (is_string($this->value))
        {
            $type   = 'anomaly.field_type.text';
            $config = ['default_value' => ''];
        }

        if (is_bool($this->value))
        {
            $type   = 'anomaly.field_type.boolean';
            $config = ['default_value' => false];
        }

        if (is_int($this->value))
        {
            $type   = 'anomaly.field_type.integer';
            $config = ['default_value' => 0];
        }

        if (is_null($this->value))
        {
            $type = false;
        }

        return [
            $this->prefix.$this->slug => [
                'type'   => $type,
                'config' => $config,
            ],
        ];
    }
}
